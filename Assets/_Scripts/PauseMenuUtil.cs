﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace HighHouse
{
    public class PauseMenuUtil : MonoBehaviour
    {
        [SerializeField]
        protected CanvasGroup menuGroup;
        [SerializeField]
        protected PlayerInteract player;
        [SerializeField]
        protected float fadeTime = 0.2f;

        protected bool menuOpened;

        private void Start()
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        IEnumerator FadeInMenu()
        {
            yield return null;
        }

        IEnumerator FadeOutMenu()
        {
            yield return null;
        }


    }
}