﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SetVolume : MonoBehaviour
{
    public AudioMixer mixer;

    [SerializeField]
    private float sliderValue;

    private void Start()
    {
        GetComponent<Slider>().value = PlayerPrefs.GetFloat("VOLUME");
    }

    private void Update()
    {
        PlayerPrefs.SetFloat("VOLUME", sliderValue);
        PlayerPrefs.Save();
    }

    public void SetLevel(float newSliderValue)
    {
        mixer.SetFloat("MusicVol", Mathf.Log10(newSliderValue) * 20);
        sliderValue = newSliderValue;
    }

}
