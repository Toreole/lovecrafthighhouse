﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseSpeed : MonoBehaviour
{
    [SerializeField]
    private float mouseSpeed;

    private void Start()
    {
        GetComponent<Slider>().value = PlayerPrefs.GetFloat("MOUSESPEED");
    }

    private void Update()
    {
        PlayerPrefs.SetFloat("MOUSESPEED", mouseSpeed);
        PlayerPrefs.Save();
    }

    public void SetMouseSpeed(float newMouseSpeed)
    {
        mouseSpeed = newMouseSpeed;
    }
}
